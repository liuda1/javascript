var quizModule = (function () {

    "use strict";

    var allQuestions,
        questionIndex = 0,
        questionAnswers,
        templates = {},
        quizDivId = "#quiz";

    Handlebars.registerHelper('shouldBeChecked', function(a, b) {
        if(a === parseInt(b)){
            return 'checked';
        }
    });

    function loadQuestionsFromJSON() {
        $.ajax({
            url: "js/questions.json",
            method: 'GET',
            async: false,
            success: function(data) {
                allQuestions = data;
                window.console.table(allQuestions);
            }
        });
    }

    function loadTemplates(){
        templates.scoreTemplate = loadTemplate("scoreTemplate");
        templates.quizFormTemplate = loadTemplate("quizFormTemplate");
    }

    function loadTemplate(name){
        var template;
        $.ajax(
            {url:"js/templates/" + name +".hbs",
                method: 'GET',
                async: false,
                success: function(data) {
                    template = Handlebars.compile(data);
                }}
        );

        return template;
    }


    function startQuiz() {
        loadQuestionsFromJSON();
        loadTemplates();
        questionAnswers = new Array(allQuestions.length);
        var singleQuestion = allQuestions[0];
        replaceQuiz(singleQuestion);
    }

    function submitAnswer() {
        if (document.quizForm.checkValidity()) {
            saveCurrentAnswer();

            questionIndex++;
            //debugger;

            if (questionIndex !== allQuestions.length) {
                var singleQuestion = allQuestions[questionIndex];
                replaceQuiz(singleQuestion, questionAnswers[questionIndex]);
            } else {
                printScore();
            }
        }
        return false;
    }

    function replaceQuiz(singleQuestion, previousAnswer){

        $(quizDivId).fadeOut(function () {
            $(quizDivId).html(createFormHTML(singleQuestion, previousAnswer));
        });
        $(quizDivId).fadeIn();
    }

    function createFormHTML(singleQuestion, previousAnswer) {

        var htmlFromTemplate = templates.quizFormTemplate({singleQuestion : singleQuestion,
            disabled : questionIndex > 0 ? undefined : ' disabled',
            previousAnswer : previousAnswer });

        return htmlFromTemplate;
    }

    function loadPreviousQuestion(){
        if(questionIndex > 0){

            saveCurrentAnswer();

            questionIndex--;
            var singleQuestion = allQuestions[questionIndex];
            var previousAnswer = questionAnswers[questionIndex];
            replaceQuiz(singleQuestion, previousAnswer);
        }
    }

    function saveCurrentAnswer() {

        var checkedAnswer = document.querySelector('input[name="answer"]:checked');
        if (checkedAnswer !== null) {
            var answer = checkedAnswer.value;
            questionAnswers[questionIndex] = answer;
        }
    }

    function printScore() {
        var score = 0;
        for(var i = 0, allQuestionLength = allQuestions.length; i < allQuestionLength; i++){
            var question = allQuestions[i];
            if (question.correctAnswer === parseInt(questionAnswers[i])) {
                score++;
            }
        }

        $(quizDivId).fadeOut(function () {
            var html = templates.scoreTemplate({score:score,totalQuestions:allQuestions.length});
            $(quizDivId).html(html);
        });
        $(quizDivId).fadeIn();

    }

    return {
        submitAnswer: submitAnswer,
        startQuiz: startQuiz,
        loadPreviousQuestion: loadPreviousQuestion
    };

}());
